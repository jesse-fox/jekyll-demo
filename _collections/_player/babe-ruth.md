---

categories: baseball
title: Babe Ruth
bio: George Herman "Babe" Ruth Jr. was an American professional baseball player whose
  career in Major League Baseball spanned 22 seasons, from 1914 through 1935. Nicknamed
  "The Bambino" and "The Sultan of Swat", he began his MLB career as a stellar left-handed
  pitcher for the Boston Red Sox, but achieved his greatest fame as a slugging outfielder
  for the New York Yankees.

---
George Herman "Babe" Ruth Jr. was an American professional baseball player

  whose career in Major League Baseball spanned 22 seasons, from 1914 through

  1935. Nicknamed "The Bambino" and "The Sultan of Swat", he began his MLB

  career as a stellar left-handed pitcher for the Boston Red Sox, but achieved

  his greatest fame as a slugging outfielder for the New York Yankees.