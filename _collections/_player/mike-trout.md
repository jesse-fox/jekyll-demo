---
categories: Baseball
title: Mike Trout
---
Michael Nelson Trout is an American professional baseball center fielder for the Los Angeles Angels of Major League Baseball. Trout is a seven-time MLB All-Star, and received the American League Most Valuable Player award in 2014 and 2016.
